
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:node/database/mysql/mysqlservice.dart';
import 'package:node/pages/home/home.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';


//CREATE TABLE IF NOT EXISTS `note` ( `ID` INT NOT NULL AUTO_INCREMENT , `color` INT(1) NOT NULL , `note` TEXT NOT NULL , PRIMARY KEY (`ID`))

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: child,
        );
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
        unselectedWidgetColor: Colors.white60,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<void> createDBIfNotExists() async {
    openDatabase(
      join(await getDatabasesPath(), 'NoteBook.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE IF NOT EXISTS notes ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, checked INTEGER(1) NOT NULL, date VARCHAR(10) NOT NULL ,color INTEGER(1) NOT NULL , note TEXT NOT NULL)",
        );
      },
      version: 1,
    );
  }

  @override
  Widget build(BuildContext context) {
    createDBIfNotExists();

    return Scaffold(
      body: FutureProvider.value(
        value: MySqlService.getAllExperience(),
        child: HomePage(),
      ),
    );
  }
}
