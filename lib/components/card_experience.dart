import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextbiggerfont.dart';
import 'dart:math';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/moudle/user.dart';
import 'package:node/pages/reading/readExperience.dart';
import 'package:node/providers/otherExperience.dart';


class CardExperience extends StatelessWidget {
  final String category;
  final String note;
  final String name;
  final User user;
  final String id;
  final ProviderExperience providerExperience;

  CardExperience({this.note, this.category,this.name,this.user,this.id,this.providerExperience});

  @override
  Widget build(BuildContext context) {

    List<Color> colors = [
      Colors.green,
      Colors.yellow,
      Colors.lime,
      Colors.redAccent,
      Colors.red,
      Colors.blue,
      Colors.deepOrange,
      Colors.purpleAccent,
      Colors.blueAccent,
      Colors.grey,
      Colors.white,
      Colors.purple,
      Colors.yellowAccent,
      Colors.amber,
    ];

    Color color = colors[Random().nextInt(14)];

    String shortDescription = note.length > 80
        ? note.substring(0, 50) + '...'
        : note;

    return Directionality(
      textDirection: TextDirection.rtl,
      child: GestureDetector(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>ReadExperience(providerExperience: providerExperience,id: id,name: name,category: category,note: note,user:user)));
        },
        child: Card(
          margin: EdgeInsets.all(8),
          color: MyColor.textColor,
          child: Container(
            decoration: BoxDecoration(
                // color: Colors.transparent,
              boxShadow: [
                BoxShadow(
                  color: color,
                  blurRadius: 5,
                  offset: Offset(0,0),
                  spreadRadius: 1
                )
              ],
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(color: color)),
            child: Container(
              decoration: BoxDecoration(
                color: MyColor.textColor,
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: [

                    Row(
                      children: [
                        SimpleTextWhit(
                          text: 'دسته بندی : ',
                        ),
                        SimpleTextBiggerFont(
                          text: category,
                        ),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Expanded(
                            child: SimpleTextWhit(
                          text: shortDescription,
                        )),
                      ],
                    ),
                    SizedBox(height: 5,),
                    Row(
                      children: [
                        Expanded(child: Text('')),
                        SimpleTextWhit(
                          text: 'نویسنده : ',
                        ),
                        SimpleTextBiggerFont(
                          text: name,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
