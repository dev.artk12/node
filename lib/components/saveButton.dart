

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/components/simpletextwhit.dart';

class SaveButton extends StatelessWidget {
  final String title;
  final Function onPress;
  final IconData icon;
  SaveButton({this.title,this.onPress,this.icon});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      textColor: Colors.white,
      color: Colors.white10,
      // color: MyColor.textColor,
      padding: EdgeInsets.only(right: 50,left: 50),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),),
      child: icon != null?Container(
        // padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          boxShadow: [
            // BoxShadow(
            //   offset: Offset(0,0),
            //   blurRadius: 10,
            //   color: Colors.white,
            // ),
          ]
        ),
        child: Row(
          children: [
            Icon(icon,color: Colors.white,),
            SimpleTextWhit(text:title),
          ],
        ),
      ):SimpleTextWhit(text:title),
      onPressed: onPress,
    );
  }
}
