

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:node/colors/colors.dart';

class DescriptionField extends StatelessWidget {
  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;
  final initText;
  final TextEditingController editingController;

  DescriptionField({this.label,this.iconData,this.onChange,this.obscureText = false,this.initText,this.editingController});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Container(
        width: MediaQuery.of(context).size.width / 1.3,
        child: TextField(
          maxLines: 10,
          controller: editingController != null? editingController:initText != null ?TextEditingController(text: initText):null,
          textAlign: TextAlign.right,
          cursorColor: Colors.white,
          onChanged: onChange,
          obscureText: obscureText,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            labelText: label,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(width: 1,color: Colors.white),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(width: 1,color: Colors.white30),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(width: 1,color: Colors.white),
            ),
            icon: iconData != null ? Icon(iconData,color: Colors.white):null,
            focusColor: Colors.white,
            labelStyle: TextStyle(
                fontSize: 12,
                color: Colors.white,
                fontFamily: "iranian_sans",
                fontWeight: FontWeight.w300
            ),
          ),
          style: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ),
      ),
    );
  }
}
