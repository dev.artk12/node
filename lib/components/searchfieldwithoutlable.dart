import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchFieldWithOutLabel extends StatelessWidget {
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;

  SearchFieldWithOutLabel(
      {this.iconData, this.onChange, this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextField(
        onChanged: onChange,
        obscureText: obscureText,
        style: TextStyle(
          color: Colors.white60,
          fontFamily: "iranian_sans",
        ),
        cursorColor: Colors.white60,
        decoration: InputDecoration(
          hintText: 'جستجو',
          hintStyle: TextStyle(
              fontSize: 12,
              color: Colors.white60,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300),
          prefixIcon: iconData != null
              ? Icon(iconData, color: Colors.white54)
              : null,
          focusColor: Colors.white60,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: Colors.white60, width: 0.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: Colors.white60, width: 0.0),
          ),
          labelStyle: TextStyle(
              fontSize: 12,
              color: Colors.white60,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300),
        ),
      ),
    );
  }
}
