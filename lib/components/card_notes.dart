import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/database/sqlite/sqlService.dart';
import 'package:node/providers/dayscontroller.dart';
import 'package:node/providers/mynotes.dart';
import 'package:provider/provider.dart';

class CardNote extends StatelessWidget {
  final DaysController daysController;
  CardNote({this.daysController});
  @override
  Widget build(BuildContext context) {
    ProviderMyNote controller = Provider.of<ProviderMyNote>(context);
    return Card(
      color: MyColor.textColor,
      // shadowColor: Colors.deepOrange,
      margin: EdgeInsets.all(10),
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              spreadRadius: 1,
              offset: Offset(0, 0),
              blurRadius: 10,
              color: Colors.black38),
        ]),
        child: Container(
          color: MyColor.textColor,
          child: Column(
            children: [
              Container(
                height: 10,
                decoration: BoxDecoration(
                  color: MyColor.noteColors[controller.color],
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(0),
                      topLeft: Radius.circular(4),
                      bottomLeft: Radius.circular(0),
                      topRight: Radius.circular(4)),
                ),
              ),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Checkbox(
                        value: controller.check == 0 ? false : true,
                        onChanged: (val) {
                          controller.onChangeCheck(val,controller.id);
                        },
                        activeColor: MyColor.textColor,
                      )),
                  Expanded(
                    flex: 8,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                              child: SimpleTextWhit(
                            text: controller.note,
                          )),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: IconButton(icon: Icon(Icons.delete,color: Colors.white24,),onPressed: (){
                      SqLiteService().delete(controller.id);
                      daysController.deleteItem();
                      // controller.delete(controller.id);
                    },)
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
