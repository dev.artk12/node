
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SimpleTextBiggerFont extends StatelessWidget {
  final String text;
  SimpleTextBiggerFont({this.text});

  @override
  Widget build(BuildContext context) {

    return   Text("$text",
      style: TextStyle(fontFamily: "iranian_sans",fontSize: 15,
          color: Colors.white,shadows: [
            Shadow(
              offset: Offset(0.0, 0.0),
              blurRadius: 20.0,
              color: Colors.white38,
            ),
          ]),);
  }
}
