
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleTextError extends StatelessWidget {
  final String text;
  final int fontSize;
  SimpleTextError({this.text,this.fontSize});


  @override
  Widget build(BuildContext context) {

    return   Text("$text",
      style: TextStyle(fontFamily: "iranian_sans",fontSize: fontSize!= null?fontSize+0.0:null,
          color: Colors.redAccent[100],shadows: [
            Shadow(
              offset: Offset(0.0, 0.0),
              blurRadius: 20.0,
              color: Colors.white38,
            ),
          ]),);
  }
}
