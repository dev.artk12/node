

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:node/colors/colors.dart';

class MiniField extends StatelessWidget {
  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;
  final TextInputType textInputType;
  final String initText;
  final TextAlign textAlign;
  final TextEditingController editingController;
  final bool readOnly;
  final Function onTap;

  MiniField({this.label,this.iconData,this.onChange,
    this.obscureText = false,this.textInputType = TextInputType.text,this.initText,
    this.textAlign = TextAlign.right,this.editingController,this.readOnly = false,this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      child: TextFormField(
        keyboardType: textInputType,
        cursorColor: Colors.white,
        textAlign: textAlign,
        readOnly: readOnly,
        onTap: onTap,
        controller: editingController,
        initialValue: editingController == null?initText:null,
        onChanged: editingController == null?onChange:null,
        obscureText: obscureText,
        decoration: InputDecoration(
          labelText: label,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: Colors.white),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: Colors.white30),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: Colors.white30),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: Colors.white30),
          ),
          icon: iconData != null ? Icon(iconData,color: Colors.white):null,
          focusColor: Colors.white,
          labelStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
        style: TextStyle(
            fontSize: 14,
            color: Colors.white,
            fontFamily: "iranian_sans",
            fontWeight: FontWeight.w300
        ),
      ),
    );
  }
}
