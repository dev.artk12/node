import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/minifield.dart';
import 'package:node/components/saveButton.dart';
import 'package:node/components/simpletexterror.dart';
import 'package:node/components/simpletextwhit.dart';

class MyDialog extends StatefulWidget {
  createState() => MyDialogSate();
}

class MyDialogSate extends State<MyDialog> {
  bool checking = false;
  bool error = false;
  bool emptyName = false;
  // String name = '';

  @override
  Widget build(BuildContext context) {

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Dialog(
        insetAnimationCurve: Curves.easeIn,
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: Container(
          height: 250,
          decoration: BoxDecoration(
            color: MyColor.textColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Card(
            color: MyColor.textColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SimpleTextWhit(
                      text: 'برای ثبت تجربه شما نیاز به اسم خاص دارید'),
                ),
                SizedBox(
                  height: 25,
                ),
                TextField(
                  // label: 'اسم',
                  readOnly: false,
                  // onChange: (val) {
                  //   // name = val;
                  // },
                ),
                SizedBox(
                  height: 20,
                ),
                error
                    ? SimpleTextError(text: 'این اسم قبلا ثبت شده')
                    : Container(),
                emptyName
                    ? SimpleTextError(text: 'لطفا نام را پر کنید')
                    : Container(),
                SizedBox(
                  height: 20,
                ),
                SaveButton(
                  title: 'ذخیره',
                  onPress: () {
                    // if(name.isEmpty){
                    //   setState(() {
                    //     emptyName = true;
                    //   });
                    // }else {
                    //   print('time to request');
                    // }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
