

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';

class AddButton extends StatelessWidget {
  final String title;
  final Function onPress;
  final IconData icon;
  AddButton({this.title,this.onPress,this.icon});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      textColor: Colors.white,
      color: MyColor.textColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),),
      child: icon != null?Container(
        width: 90,
        child: Row(
          children: [
            Icon(icon,color: Colors.white,),
            SimpleTextWhit(text:title),
          ],
        ),
      ):SimpleTextWhit(text:title),
      onPressed: onPress,
    );
  }
}
