
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleTextWhit extends StatelessWidget {
  final String text;
  final int fontSize;
  final Color textColor;
  SimpleTextWhit({this.text,this.fontSize,this.textColor});


  @override
  Widget build(BuildContext context) {

    return   Text("$text",
      style: TextStyle(fontFamily: "iranian_sans",fontSize: fontSize!= null?fontSize+0.0:null,
          color: textColor == null ? Colors.white:textColor,shadows: [
            Shadow(
              offset: Offset(0.0, 0.0),
              blurRadius: 5.0,
              color: Colors.white38,
            ),
          ]),);
  }
}
