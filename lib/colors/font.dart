class MyFont {

  static final List<String> myFonts = [
    'iranian_sans',
    'nazanin',
    'koodak',
    'b_lotus',
    'far_mitra',
    'tahoma',
  ];

  static List<DropdownListItem> getListDropDown(){
    List<DropdownListItem> dropdownListItem = [];
    for(int i = 0 ; i < myFonts.length;i++){
      dropdownListItem.add(DropdownListItem(val: i,name: myFonts[i]));
    }
    return dropdownListItem;
  }
}

class DropdownListItem{
  int val;
  String name;
  DropdownListItem({this.name,this.val});
}
