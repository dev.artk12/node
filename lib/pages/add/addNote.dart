import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/descriptionfield.dart';
import 'package:node/components/saveButton.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/database/sqlite/sqlService.dart';
import 'package:node/moudle/note.dart';
import 'package:node/providers/addnote.dart';
import 'package:node/providers/mynotes.dart';
import 'package:provider/provider.dart';

class AddNote extends StatelessWidget {
  // final ProviderMyNote myNoteController;
  final String dateTime;
  AddNote({this.dateTime});
  List<Widget> colorList(ProviderAddNote controller){
    return List.generate(3, (index) {
      return Container(
        margin: EdgeInsets.all(5),
        child: InkWell(
          onTap: (){
            controller.onChangeColor(index);
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                CircleAvatar(
                  backgroundColor: MyColor.noteColors[index],
                  radius: index == controller.color? 20: 15,
                ),
                SimpleTextWhit(text: MyColor.myNoteText[index],textColor: MyColor.noteColors[index])
              ],
            ),
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    ProviderAddNote controller = Provider.of<ProviderAddNote>(context);

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: MyColor.textColor,
        appBar: AppBar(
          centerTitle: true,
          title: SimpleTextWhit(text: 'اضافه یادداشت'),
          backgroundColor: MyColor.textColor,
        ),
        body: Builder(
          builder:(context)=> SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: MediaQuery.of(context).size.width/1.2,
                  decoration: BoxDecoration(
                    color: MyColor.textColor,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black12,
                        spreadRadius: 10,
                        blurRadius: 7,
                        offset: Offset(0, 5),
                      )
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight:Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            SizedBox(width: 5,),
                            SimpleTextWhit(text:'متن شما'),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Center(
                            child: DescriptionField(
                          onChange: controller.onChangeNote,
                        )),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  margin: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: MyColor.textColor,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black12,
                        spreadRadius: 10,
                        blurRadius: 7,
                        offset: Offset(0, 5),
                      )
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight:Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SimpleTextWhit(text:'انتخاب رنگ :   '),
                      Row(children: colorList(controller),)
                    ],
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
                SaveButton(
                  title: 'اضافه',
                  onPress: () async{
                    if(controller.note.isEmpty){
                      SqLiteService.showStatus('لطفا متن رو وارد کنین.', context);
                    }else{
                      Map<String,dynamic> map = Note(dateTime: dateTime,color: controller.color,note: controller.note).toInsertMap();
                      int res = await SqLiteService().insert(map);
                      Navigator.pop(context,Note(id: res,color: controller.color,dateTime: dateTime,note: controller.note,check: 0));
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
