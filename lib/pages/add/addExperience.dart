import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/descriptionfield.dart';
import 'package:node/components/dialog_userName.dart';
import 'package:node/components/minifield.dart';
import 'package:node/components/saveButton.dart';
import 'package:node/components/simpletexterror.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/database/mysql/mysqlservice.dart';
import 'package:node/database/sharedpreferences/sharedprefrences.dart';
import 'package:node/moudle/experience.dart';
import 'package:node/moudle/user.dart';
import 'package:node/providers/addExperience.dart';
import 'package:node/providers/otherExperience.dart';
import 'package:provider/provider.dart';

class AddExperience extends StatelessWidget {
  final ProviderExperience experiencePageController;
  AddExperience({this.experiencePageController});

  String getInitText(User user ){
      if(user == null){
          print('here');
        return '';
      }else if(user.name == 'null'){
        return '';
      }else{
        return user.name;
      }

  }

  void updateUser(User user , ProviderAddExperience controller){
    controller.updateUser(user);
  }

  @override
  Widget build(BuildContext context) {
    ProviderAddExperience controller =
        Provider.of<ProviderAddExperience>(context);
    User user = Provider.of<User>(context);
    if(user != null){
      controller.updateUser(user);
    }
    // if(user != null){
    //   print(user.name);
    // }
    // print(user.id);
    // WidgetsBinding.instance
    //     .addPostFrameCallback((_) => updateUser(user,controller));

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: SimpleTextWhit(
            text: 'اشتراک تجربیات',
          ),
          centerTitle: true,
          backgroundColor: MyColor.textColor,
        ),
        backgroundColor: MyColor.textColor,
        body: user == null?CircularProgressIndicator():SingleChildScrollView(
          child: SafeArea(
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 1.4,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: MyColor.textColor,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              spreadRadius: 10,
                              blurRadius: 7,
                              offset: Offset(0, 5),
                            )
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            bottomLeft: Radius.circular(50),
                          ),
                        ),
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  SimpleTextWhit(text: 'موضوع :'),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  MiniField(
                                    onChange: controller.onChangeCategory,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  SimpleTextWhit(text: 'اسم :    '),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  MiniField(
                                    onChange: controller.onChangeName,
                                    initText:  controller.user.name == 'null'?'':controller.user.name,
                                    readOnly: controller.user == null ||
                                            controller.user.id == -1
                                        ? false
                                        : true,
                                    onTap: () {
                                      // if(user.id == -1){
                                      //   showDialogName(user, context);
                                      // }
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              controller.user == null ||
                                      controller.user.id == -1
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SimpleTextError(
                                          text:
                                              'شما اولین باره تجربه خودتون رو وارد میکنید و این اسم برای همیشه برای شما ثبت خواهد شد.'),
                                    )
                                  : Container(),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 25,
                                  ),
                                  SimpleTextWhit(text: 'متن ارسالی شما'),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 20,
                                  ),
                                  DescriptionField(
                                    onChange: controller.onChangeNote,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Builder(
                  builder: (context) => Container(
                    height: MediaQuery.of(context).size.height - 120,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: SaveButton(
                        title: 'ارسال',
                        onPress: () async {
                          if (controller.category.isEmpty) {
                            MySqlService.showStatus(
                                'موضوع رو وارد نکردی', context);
                          } else if (controller.name.isEmpty) {
                            MySqlService.showStatus(
                                'اسم رو وارد نکردی', context);
                          } else if (controller.note.isEmpty) {
                            MySqlService.showStatus(
                                'متن رو وارد نکردی', context);
                          } else {
                            User currentUser;
                            String userRes;
                            if (user.id == -1) {
                              // get from database
                              userRes = await MySqlService.insert(
                                  {'name': controller.name}, 'addnoteUser.php');
                              currentUser = User(
                                  id: int.parse(userRes),
                                  name: controller.name);
                              print(userRes);
                              if (currentUser.id != -2) {
                                MySharedPreferences.insertUser(
                                    currentUser.id, currentUser.name);
                              }
                            } else {
                              currentUser = user;
                            }

                            if (currentUser.id == -2) {
                              MySqlService.showStatus(
                                  'این اسم قبلا ثبت شده لطفا اسم دیگه ای رو انتخاب کنید.',
                                  context);
                            } else {
                              // MySharedPreferences.insertUser(currentUser.id, currentUser.name);

                              String res = await MySqlService.insert({
                                'name': currentUser.name,
                                'category': controller.category,
                                'note': controller.note
                              }, 'addExperience.php');
                              MySqlService.showStatus(
                                  'در حال ذخیره...', context);
                              if (res == '-1') {
                                MySqlService.showStatus(
                                    'مشکل در اتصال اینترنت', context);
                              } else {
                                experiencePageController.addExperience(
                                    Experience(
                                        id: res,
                                        note: controller.note,
                                        name: controller.name,
                                        category: controller.category));
                                Navigator.pop(context);
                              }
                            }
                          }
                        },
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
