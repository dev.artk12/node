import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/database/sharedpreferences/sharedprefrences.dart';
import 'package:node/moudle/experience.dart';
import 'package:node/moudle/note.dart';
import 'package:node/pages/home/days.dart';
import 'package:node/pages/home/myNotes.dart';
import 'package:node/pages/home/otherExperience.dart';
import 'package:node/providers/dayscontroller.dart';
import 'package:node/providers/mynotes.dart';
import 'package:node/providers/otherExperience.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // List<Note> notes = Provider.of<List<Note>>(context) ?? [];
    // List<Experience> experiences = Provider.of<List<Experience>>(context) ?? [];
    // ProviderMyNote providerMyNote = new ProviderMyNote(notes);
    // ProviderExperience providerExperience = new ProviderExperience(experiences);

    return ChangeNotifierProvider.value(value: DaysController(), child: Days());

    // return DefaultTabController(
    //   length: 1,
    //   child: Scaffold(
    //     bottomNavigationBar: PreferredSize(
    //       preferredSize: Size.fromHeight(kToolbarHeight),
    //       child: SafeArea(
    //         child: Container(
    //           color: MyColor.textColor,
    //           child: TabBar(
    //             indicatorColor: Colors.white,
    //             labelColor: Colors.white,
    //             indicatorSize: TabBarIndicatorSize.tab,
    //             indicatorPadding: EdgeInsets.all(5.0),
    //             tabs: [
    //               Tab(
    //                 text: 'یادداشت ها من',
    //               ),
    //               // Tab(
    //               //   text: 'تجربیات',
    //               // ),
    //             ],
    //           ),
    //         ),
    //       ),
    //     ),
    //     body: TabBarView(
    //       children: [
    //         ChangeNotifierProvider.value(
    //           value: providerMyNote,
    //           child: MyNotes(),
    //         ),
    //         // FutureProvider.value(
    //         //   value: MySharedPreferences.getUser(),
    //         //     child: ChangeNotifierProvider.value(
    //         //         value: providerExperience, child: UserExperience())),
    //       ],
    //     ),
    //   ),
    // );
  }
}
