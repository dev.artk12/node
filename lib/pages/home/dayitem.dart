import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/database/sqlite/sqlService.dart';
import 'package:node/moudle/datetimefunction.dart';
import 'package:node/pages/home/daynote.dart';
import 'package:node/providers/dayscontroller.dart';
import 'package:provider/provider.dart';
import 'package:shamsi_date/shamsi_date.dart';

class DayItem extends StatelessWidget {
  final int index;
  final DaysController daysController;
  DayItem({this.index, this.daysController});
  @override
  Widget build(BuildContext context) {
    DateTime dateTime;

    if (index == 60) {
      dateTime = DateTime.now();
    } else if (index < 60) {
      dateTime = DateTime.now().subtract(Duration(days: 60 - index));
    } else {
      dateTime = DateTime.now().add(Duration(days: index - 60));
    }
    Jalali j =
        Gregorian(dateTime.year, dateTime.month, dateTime.day).toJalali();
    // daysController.onChangePage(index);
    //1440
    return Scaffold(
      backgroundColor: MyColor.textColor,
      body: SafeArea(
        child: FutureProvider.value(
            value: SqLiteService().getNotes(DateTimeFunction.jalaliToString(j)),
            child: DayNotes(daysController:daysController,dateTime: DateTimeFunction.jalaliToString(j))),
      ),
    );
  }
}
