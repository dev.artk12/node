import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/card_experience.dart';
import 'package:node/components/searchfieldwithoutlable.dart';
import 'package:node/database/sharedpreferences/sharedprefrences.dart';
import 'package:node/moudle/user.dart';
import 'package:node/pages/add/addExperience.dart';
import 'package:node/providers/addExperience.dart';
import 'package:node/providers/otherExperience.dart';
import 'package:provider/provider.dart';

class UserExperience extends StatelessWidget {
  Widget searchWidgets(BuildContext context, ProviderExperience controller) {
    return Container(
      height: 70,
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: MyColor.textColor,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 15.0,
            offset: Offset(0.0, 0.75),
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 5,
              child: Center(
                  child: SearchFieldWithOutLabel(
                onChange: controller.onChangeSearch,
                iconData: Icons.search,
              )),
            ),
            Expanded(
              flex: 1,
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: IconButton(
                        onPressed: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => FutureProvider.value(
                                value: MySharedPreferences.getUser(),
                                child: ChangeNotifierProvider.value(
                                  value: ProviderAddExperience(),
                                  child: AddExperience(
                                    experiencePageController: controller,
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                        icon: Icon(
                          Icons.add,
                          color: Colors.white54,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    ProviderExperience controller = Provider.of<ProviderExperience>(context);
    User user = Provider.of<User>(context);

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: MyColor.textColor,
        body: SafeArea(
          child: Container(
            child: ListView.builder(
              itemCount: controller.searchExperience.length + 1,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return searchWidgets(context, controller);
                } else {
                  return CardExperience(
                    note: controller.searchExperience[index - 1].note,
                    category: controller.searchExperience[index - 1].category,
                    name: controller.searchExperience[index - 1].name,
                    user: user,
                    id:controller.searchExperience[index -1].id,
                    providerExperience: controller,
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
