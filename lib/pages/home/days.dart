import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/moudle/datetimefunction.dart';
import 'package:node/moudle/note.dart';
import 'package:node/pages/add/addNote.dart';
import 'package:node/providers/addnote.dart';
import 'package:node/providers/dayscontroller.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:provider/provider.dart';
import 'dayitem.dart';

class Days extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PageController pageController = new PageController(initialPage: 60);
    DaysController daysController = Provider.of<DaysController>(context);

    return Scaffold(
        backgroundColor: MyColor.textColor,
        appBar: AppBar(
          backgroundColor: MyColor.textColor,
          centerTitle: true,
          title: SimpleTextWhit(
            text: NumberUtility.changeDigit(
                daysController.date, NumStrLanguage.Farsi),
          ),
        ),
        body: Padding(
          padding:
              const EdgeInsets.only(left: 5.0, right: 5.0, bottom: 20, top: 5),
          child: PageView.builder(
            onPageChanged: daysController.onChangePage,
            controller: pageController,
            itemBuilder: (context, index) => DayItem(
              daysController: daysController,
              index: index,
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation
            .startDocked, //specify the location of the FAB
        floatingActionButton: CircleAvatar(
          radius: 32,
          backgroundColor: MyColor.textColor,
          child: Padding(
            padding: EdgeInsets.all(1),
            child: FloatingActionButton(
              backgroundColor: MyColor.textColor,
              onPressed: () async{
                Note note = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider.value(
                      value: ProviderAddNote(),
                      child: AddNote(
                          dateTime: DateTimeFunction.jalaliToString(
                              daysController.jalali)),
                    ),
                  ),
                );
                if(note != null){
                  daysController.addToExtraNote(note);
                }
              },
              child: Container(
                margin: EdgeInsets.all(15.0),
                child: Icon(Icons.add),
              ),
              elevation: 4.0,
            ),
          ),
        ),
    );
  }
}
