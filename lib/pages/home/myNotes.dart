import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/card_notes.dart';
import 'package:node/components/dialog_userName.dart';
import 'package:node/components/searchfieldwithoutlable.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/database/mysql/mysqlservice.dart';
import 'package:node/pages/add/addNote.dart';
import 'package:node/pages/home/days.dart';
import 'package:node/providers/addnote.dart';
import 'package:node/providers/mynotes.dart';
import 'package:provider/provider.dart';

class MyNotes extends StatelessWidget {
  // Widget searchWidgets(BuildContext context, ProviderMyNote controller) {
  //   return Container(
  //     height: 70,
  //     margin: EdgeInsets.only(bottom: 10),
  //     decoration: BoxDecoration(
  //       color: MyColor.textColor,
  //       boxShadow: [
  //         BoxShadow(
  //           color: Colors.black26,
  //           blurRadius: 15.0,
  //           offset: Offset(0.0, 0.75),
  //         )
  //       ],
  //     ),
  //     child: Padding(
  //       padding: const EdgeInsets.all(15.0),
  //       child: Row(
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           Expanded(
  //             flex: 5,
  //             child: Center(
  //                 child: SearchFieldWithOutLabel(
  //               iconData: Icons.search,
  //               onChange: controller.onChangeSearch,
  //             )),
  //           ),
  //           Expanded(
  //             flex: 1,
  //             child: Center(
  //               child: IconButton(
  //                 onPressed: () async {
  //                   // String res = await MySqlService.hello();
  //                   // print(res);
  //                   Navigator.push(
  //                     context,
  //                     MaterialPageRoute(
  //                       builder: (context) => ChangeNotifierProvider.value(
  //                         value: ProviderAddNote(),
  //                         child: AddNote(myNoteController: controller),
  //                       ),
  //                     ),
  //                   );
  //                 },
  //                 icon: Icon(
  //                   Icons.add,
  //                   color: Colors.white54,
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    ProviderMyNote controller = Provider.of<ProviderMyNote>(context);

    return Scaffold(
      backgroundColor: MyColor.textColor,
      body: Days(),
    );

    // return Directionality(
    //   textDirection: TextDirection.rtl,
    //   child: Scaffold(
    //     backgroundColor: MyColor.textColor,
    //     body: SafeArea(
    //       child: Container(
    //         child: controller.myNotes.isEmpty
    //             ? Stack(
    //                 children: [
    //                   // Align(
    //                   //     alignment: Alignment.topCenter,
    //                   //     child: searchWidgets(context, controller)),
    //                   Align(
    //                     alignment: Alignment.center,
    //                     child: Container(
    //                       padding: EdgeInsets.all(17),
    //                       child: SimpleTextWhit(
    //                           fontSize: 10,
    //                           text:
    //                               ' هیچ یادداشتی ندارید میتونید از دکمه (+) بالا سمت چپ یادداشت اضاف کنید.'),
    //                     ),
    //                   ),
    //                 ],
    //               )
    //             : ListView.builder(
    //                 itemCount: controller.searchNotes.length,
    //                 itemBuilder: (context, index) {
    //                   // if (index == 0) {
    //                   //   return searchWidgets(context, controller);
    //                   // } else {
    //                     return CardNote(
    //                       color: controller.searchNotes[index].color,
    //                       id: controller.searchNotes[index].id,
    //                       note: controller.searchNotes[index].note,
    //                       check: controller.searchNotes[index].check,
    //                       controller: controller,
    //                       index: index,
    //                     );
    //                   }
    //                   // return null;
    //                 // },
    //               ),
    //       ),
    //     ),
    //   ),
    // );
  }
}
