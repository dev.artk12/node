import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/card_notes.dart';
import 'package:node/database/mysql/mysqlservice.dart';
import 'package:node/moudle/note.dart';
import 'package:node/providers/dayscontroller.dart';
import 'package:node/providers/mynotes.dart';
import 'package:provider/provider.dart';

class DayNotes extends StatelessWidget {
  final DaysController daysController;
  final String dateTime;
  DayNotes({this.daysController,this.dateTime});
  @override
  Widget build(BuildContext context) {
    List<Note> notes = Provider.of<List<Note>>(context);
    if(notes != null){
      notes.sort((a,b)=>a.color.compareTo(b.color));
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(
          color: MyColor.textColor,
          borderRadius: BorderRadius.circular(20),
        ),
        child: notes == null
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                itemCount: notes.length,
                itemBuilder: (c, i) => ChangeNotifierProvider.value(
                  value: ProviderMyNote(
                    color: notes[i].color,
                    id: notes[i].id,
                    note: notes[i].note,
                    check: notes[i].check,
                  ),
                  child: CardNote(daysController: daysController,),
                ),
              ),
      ),
    );
  }
}
