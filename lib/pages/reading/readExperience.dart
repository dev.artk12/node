import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/database/mysql/mysqlservice.dart';
import 'package:node/moudle/user.dart';
import 'package:node/providers/otherExperience.dart';

class ReadExperience extends StatelessWidget {
  final String category;
  final String name;
  final String note;
  final String id;
  final User user;
  final ProviderExperience providerExperience;

  ReadExperience({this.category, this.note, this.name, this.user,this.id,this.providerExperience});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: MyColor.textColor,
        body: SingleChildScrollView(
          child: SafeArea(
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minHeight: MediaQuery.of(context).size.height / 1.4,
                        ),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: MyColor.textColor,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 10,
                                blurRadius: 7,
                                offset: Offset(0, 5),
                              )
                            ],
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50),
                              bottomLeft: Radius.circular(50),
                            ),
                          ),
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        SimpleTextWhit(text: 'موضوع :'),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        SimpleTextWhit(
                                          text: category,
                                        ),
                                      ],
                                    ),
                                    Expanded(
                                      child: Text(''),
                                    ),
                                    user.name != name
                                        ? Container()
                                        : Builder(
                                          builder:(context)=> IconButton(
                                              icon: Icon(
                                                Icons.delete,
                                                color: Colors.redAccent[100],
                                              ),
                                              onPressed: () async{
                                                String res = await MySqlService.delete('deleteExperience.php', id);
                                                MySqlService.showStatus('لطفا صبر کنید...', context);
                                                if(res=='done'){
                                                  providerExperience.remove(id);
                                                  Navigator.pop(context);
                                                }else if(res == '-1'){
                                                  MySqlService.showStatus('مشکل در اتصال اینترنت...', context);
                                                }
                                              },
                                            ),
                                        ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    SimpleTextWhit(text: 'اسم :    '),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    SimpleTextWhit(
                                      text: name,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                // Row(
                                //   children: [
                                //     SizedBox(
                                //       width: 25,
                                //     ),
                                //     SimpleTextWhit(text: 'متن'),
                                //   ],
                                // ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: SimpleTextWhit(
                                        text: note,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
