import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/moudle/experience.dart';

class MySqlService {
  static String baseUrl = "http://www.realestatedealer.ir/node/";

  static void showStatus(String message,BuildContext context){
    Scaffold.of(context).showSnackBar(SnackBar(backgroundColor: MyColor.textColor,content: SimpleTextWhit(text:message),));
  }

  static Future<String> insert(Map<String,String> map , String url)async{
    int timeout = 5;
    try {
      final response = await http.post(baseUrl+url,body: map).
      timeout(Duration(seconds: timeout));
      print(response.statusCode);
      if (response.statusCode == 200) {
       return response.body;
      } else {
        return '-1';
      }
    } on TimeoutException catch (e) {
      print(e.message);
      return '-1';
    } on SocketException catch (e) {
      print(e.message);
      return '-1';
    } on Error catch (e) {
      print(e);
      return '-1';
    }
  }

  static Future<String> update(Map<String,String> map , String url)async{
    final response = await http.post(baseUrl+url,body: map);
    return response.body;
  }


  static Future<List<Experience>> getAllExperience()async{
    final response = await http.post(baseUrl+'getUserExperience.php');
    return Experience().getExpereinceListFromJson(response.body);
  }

  static Future<String> delete(String url,String id)async{
    final response = await http.post(baseUrl+url,body: {'id':id});
    if(response.statusCode == 200){
      return response.body;
    }else{
      return '-1';
    }
  }

}