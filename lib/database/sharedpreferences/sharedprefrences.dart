import 'package:node/moudle/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MySharedPreferences {

  static Future<String> getName()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString('name')??'null';
    return name;
  }
  static Future<int> getId()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int id = prefs.getInt('id')??-1;
    return id;
  }

  static Future<User> getUser()async{
    String name = await getName();
    int id = await getId();
    return User(id: id,name: name);
  }

  static insertUser(int id,String name )async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('id', id);
    prefs.setString('name', name);
  }

  static deleteUser()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('id');
    prefs.remove('name');
  }
}