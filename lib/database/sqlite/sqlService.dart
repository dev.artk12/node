
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:node/colors/colors.dart';
import 'package:node/components/simpletextwhit.dart';
import 'package:node/moudle/note.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';


class SqLiteService {

  static void showStatus(String message,BuildContext context){
    Scaffold.of(context).showSnackBar(SnackBar(backgroundColor: MyColor.textColor,content: SimpleTextWhit(text:'در حال ذخیره ....'),));
  }

  Future<int> insert(Map<String,dynamic> map)async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'NoteBook.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = database;
    dynamic res = await db.insert('notes', map,conflictAlgorithm: ConflictAlgorithm.replace);
    return res;
  }

  Future<void> update(Map<String,dynamic> map,int id)async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'NoteBook.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = database;
    await db.update('notes', map,where: 'ID = ?',whereArgs: [id]);
  }

  Future<void> delete(int id)async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'NoteBook.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = database;
    await db.delete('notes',where: 'ID = ?',whereArgs: [id]);
  }

  Future<List<Note>> getNotes(String dateTime)async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'NoteBook.db');
    Database db = await openDatabase(path, version: 1);
    final List<Map<String, dynamic>> maps = await db.query('notes',where: 'date = ?',whereArgs: [dateTime]);
    return Note.getListNoteFromDB(maps);
  }

}