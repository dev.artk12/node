class Note {
  int id;
  int color;
  String note;
  int check;
  String dateTime;

  Note({this.color, this.id, this.note,this.check,this.dateTime});

  Map<String, dynamic> toInsertMap() {
    return {
      'color': color,
      'note': note,
      'date':dateTime,
      'checked': 0,
    };
  }

  static List<Note> getListNoteFromDB(List<Map<String, dynamic>> map) {
    return List.generate(
      map.length,
      (index) {
        return Note(
            id: map[index]['ID'],
            note: map[index]['note'],
            color: map[index]['color'],
            check: map[index]['checked']);
      },
    );
  }
}
