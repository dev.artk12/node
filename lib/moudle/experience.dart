import 'dart:convert';

class Experience {
  String id;
  String name;
  String note;
  String category;

  Experience({this.name, this.id, this.note,this.category});

  Map<String, dynamic> toInsertMap() {
    return {
      'name': name,
      'note': note,
      'category': category,
    };
  }


  static List<Experience> getListNoteFromDB(List<Map<String, dynamic>> map) {
    return List.generate(
      map.length,
      (index) {
        return Experience(
            id: map[index]['ID'],
            note: map[index]['note'],
            category: map[index]['category'],
            name: map[index]['name']);
      },
    );
  }

  Experience fromJson(Map<String,dynamic> map){
    return Experience(
        id: map['ID'],
        note: map['note'],
        category: map['category'],
        name: map['name']);
  }

  List<Experience> getExpereinceListFromJson(String body){
    return (json.decode(body) as List).map((e) => fromJson(e)).toList();
  }
}
