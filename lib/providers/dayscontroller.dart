
import 'package:flutter/cupertino.dart';
import 'package:node/moudle/datetimefunction.dart';
import 'package:node/moudle/note.dart';
import 'package:shamsi_date/shamsi_date.dart';

class DaysController extends ChangeNotifier{

  int i = 60;
  DateTime dateTime = new DateTime.now();
  Jalali jalali;
  String date;
  List<Note> extraNotes =[];

  void addToExtraNote(Note note){
    extraNotes.add(note);
    notifyListeners();
  }

  void deleteItem(){
    notifyListeners();
  }

  void deleteExtraTimeNote(int id){
    extraNotes.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  DaysController(){
    jalali = new Gregorian(dateTime.year,dateTime.month,dateTime.day,).toJalali();
    date = '${jalali.day} ${DateTimeFunction.month[jalali.month-1]} ${jalali.year}';
  }

  onChangePage(int index){

    if(index == 60){
      dateTime = DateTime.now();
    }else if (index < 60){
      dateTime = DateTime.now().subtract(Duration(days: 60-index));
    }else{
      dateTime = DateTime.now().add(Duration(days: index-60));
    }
    jalali = Gregorian(dateTime.year,dateTime.month,dateTime.day).toJalali();
    date = '${jalali.day} ${DateTimeFunction.month[jalali.month-1]} ${jalali.year}';
    print(date);
    notifyListeners();
  }


}