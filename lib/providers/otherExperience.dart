import 'package:flutter/cupertino.dart';
import 'package:node/moudle/experience.dart';

class ProviderExperience extends ChangeNotifier {

  List<Experience> experiences = [];
  List<Experience> searchExperience = [];

  ProviderExperience(List<Experience> experience) {
    this.experiences = experience;
    this.searchExperience = this.experiences;
  }

  void addExperience(Experience experience) {
    experiences.add(experience);
    notifyListeners();
  }

  void onChangeSearch(String val) {
    searchExperience = experiences
        .where((element) =>
            element.note.contains(val) ||
            element.category.contains(val) ||
            element.name.contains(val))
        .toList();
    notifyListeners();
  }

  void remove(String id){
    experiences.removeWhere((element) => element.id == id);
    searchExperience.removeWhere((element) => element.id == id);
    notifyListeners();
  }
}
