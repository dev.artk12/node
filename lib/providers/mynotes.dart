
import 'package:flutter/cupertino.dart';
import 'package:node/database/sqlite/sqlService.dart';
import 'package:node/moudle/note.dart';

class ProviderMyNote extends ChangeNotifier{

  String note;
  int id;
  int check;
  int color;

  ProviderMyNote({this.check,this.note,this.color,this.id});
  // String
  // List<Note> myNotes = [];
  // List<Note> searchNotes = [];

  // ProviderMyNote(List<Note> myNotes){
  //   // this.myNotes = myNotes;
  //   // this.searchNotes = this.myNotes;
  // }

  // void addNote(Note note){
  //   myNotes.add(note);
  //   notifyListeners();
  // }
  //
  // void delete(int id){
  //   myNotes.removeWhere((element) => element.id == id);
  //   notifyListeners();
  // }

  void onChangeCheck(bool val,int id){
    int i = val?1:0;
    check = i;
    SqLiteService().update({'checked':i}, id);

    notifyListeners();
  }

  // void onChangeSearch(String val){
  //   searchNotes = myNotes.where((element) => element.note.contains(val)).toList();
  //   notifyListeners();
  // }

}