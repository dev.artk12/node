import 'package:flutter/cupertino.dart';
import 'package:node/moudle/user.dart';

class ProviderAddExperience extends ChangeNotifier {
  int id;
  String name = '';
  String category= '';
  String note= '';
  User user = User(name: 'null',id: -1);

  void updateUser(User user){
    if(user == null){
      user = User(name: 'null',id: -1);
    }
    if(user != null){
      if(user.name != 'null'){
        this.name = user.name;
      }
    }
    this.user = user;
  }

  void onChangeNote(String val) {
    note = val;
  }

  void onChangeCategory(String val) {
    category = val;
  }

  void onChangeName(String val) {
    name = val;
  }
}
