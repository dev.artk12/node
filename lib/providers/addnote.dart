
import 'package:flutter/cupertino.dart';

class ProviderAddNote extends ChangeNotifier{

  int id;
  String note;
  int color = 0;

  void onChangeNote(String val){
    this.note = val;
  }

  void onChangeColor(int i ){
    this.color = i;
    notifyListeners();
  }

}